//数组置换
function frequencyContrast(frequencyArray, key) {
	var result = [];
	for(var i = 0; i < 26; i++) {
		if(i - key < 0) {
			result[26 + (i - key)] = frequencyArray[i];
		}
		if(i - key >= 0) {
			result[i - key] = frequencyArray[i];
		}
	}
	return result;
}

//凯撒偏移
function caesarLettter(string, key) {
	var newString = "";
	for(var i = 0; i < string.length; i++) {
		if((string.charCodeAt(i) - key) < 65) {
			newString += String.fromCharCode(90 - (key - (string.charCodeAt(i) - 64))) ;
		}
		if((string.charCodeAt(i) - key) >= 65) {
			newString += String.fromCharCode(string.charCodeAt(i) - key);
		}
	}
	return newString;
}

//概率计算
function difference(list1, list2) {
	var difference = 0;
	for(var i = 0; i < list1.length; i++) {
		difference += Math.abs(list1[i] - list2[i]);
	}
	return difference;
}